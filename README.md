# README #

### What is this repository for? ###

* Atlassian Jira Test to deploy Jira server using cloudformation template and ansible.

### How do I get set up? ###

* Creates stack using Cloudformation template jira.yml file from this repo in a given VPC
* Provisions EC2 Instance(t3.large) using Amazon Linux AMI, Security Groups(HTTP & HTTPS allowed for all[not secured]), Master & Read Replica RDS Instances with MySQL Community engine. 
* Once jira.yml is uploaded through Cloudformation, kindly provide 
	-DbName
	-DbPassword
	-VpcId
	-SubnetId's
	-ImageId
* Stack will take 15/20 mins to create.
* Ansible playbook runs automatically after the AWS CloudFormation template has provisioned the resources.

### Assumptions ###

* SG's are open for testing purposes
* Add manually Inboud rule in SG to SSH into instance if needed. 
* Used personal KeyPair, change it appropriately in yml file.

### Who do I talk to? ###

* Repo owner or admin

### Note ###
* This repo is solely used for testing 